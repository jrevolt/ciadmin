import {gitlab, Gitlab} from "./gitlab";
import {cfg} from "./cfg";

test('parse project ref', () => {
  let ref = gitlab.parseProjectPath("group/subgroup/project/repo:branch")
  expect(ref.group).toBe('group:subgroup')
  expect(ref.project).toBe('project')
  expect(ref.repo).toBe('repo')
  expect(ref.branch).toBe('branch')
})

test('resolve repo', () => {
  let ref = gitlab.parseProjectPath('gtest/ptest/rtest')
  let r = gitlab.resolveRepository(ref)
  expect(r.name).toBe('rtest')
  expect(r.project.name).toBe('ptest')
  expect(r.project.group.name).toBe('gtest')
})

test('parse project ref: no branch', () => {
  let ref = gitlab.parseProjectPath("group/subgroup/project/repo")
  expect(ref.group).toBe('group:subgroup')
  expect(ref.project).toBe('project')
  expect(ref.repo).toBe('repo')
  expect(ref.branch).toBe(undefined)
})

test('parse project ref: missing repo', () => {
  expect(() => {
    gitlab.parseProjectPath("group/project")
  }).toThrow(Error)
})
