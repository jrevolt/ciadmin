import {anotherCommand, Options, prepareSonar} from "./commands";
import {configureLogging, log} from "./log";
import {reportedErrors} from "./utils";
import extend from "extend";
import {Command} from "commander";
import * as version from './version.json'

export const is_unit_test : boolean = process.env.NODE_ENV == "test" && process.env.JEST_WORKER_ID != undefined
export const is_cli = !is_unit_test

export class Main {

  readonly program: Command = (new Command('ciadmin')
    .version(this.versionString())
    .option('-d, --debug')
  ) as Command

  command : Command = this.program

  options() { return this.program.opts() as Options }

  async run(args?: string[]) {
    if (args) args.unshift('dummy-exe-name', 'dummy-script-name')
    if (!args) args = process.argv

    this.registerCommands()

    configureLogging(this)

    await this.program.parseAsync(args)
  }

  registerCommands() {
    this.program
      .command('prepare-sonar [project]')
      .option('--force')
      .action(this.wrap(prepareSonar))
    this.program
      .command('another-command')
      .action(this.wrap(anotherCommand))
  }

  wrap(action) {
    const main = this;
    return async function () {
      let started = Date.now();
      let error: Error | undefined;
      let cmd = main.command = arguments[arguments.length - 1]

      try {
        let opts: Options = extend({}, cmd.parent.opts(), cmd.opts())
        let args: any[] = [opts]
        for (let i = 0; i < arguments.length - 1; i++) args.push(arguments[i])

        log.level = opts.debug ? 'debug' : 'info'

        log.info(`${main.program.name()} ${main.versionString()})`)
        log.info('Executing %s (%s)', cmd.name(), action.name);

        // @ts-ignore
        await action.apply(this, args);

      } catch (e) {
        error = e;
        process.exitCode = 3;

      } finally {
        let elapsed = Date.now() - started;
        let logm = error || reportedErrors > 0 ? log.error : log.info;
        let msg = reportedErrors > 0 ? `There were ${reportedErrors} errors reported.` : '';
        let err = error ? error.stack : '';
        logm('Finished command %s (%s) in %d msec. %s %s', cmd.name(), action.name, elapsed, msg, err);
        log.level = 'info'
        //log.end();
      }
    }
  }

  versionString() : string {
    return `${version.FullSemVer} (${version.CommitDate}, ${version.ShortSha})`
  }

}

export const main : Main = new Main()

if (is_cli) main.run().then()

