import {Main} from "./main";

const main = new Main()

test('main', async () => {
  await main.run(['prepare-sonar', 'gtest/ptest/rtest:master'])
})