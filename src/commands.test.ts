import {prepareSonar, PrepareSonarOptions} from './commands'

test('prepare-sonar', async () => {
  await prepareSonar({} as PrepareSonarOptions ,'gtest/ptest/rtest:master')
})