import extend from 'extend'
import {rethrow, xrequest} from "./utils";
import {cfg, Repository} from "./cfg";

export interface ProjectRef {
  group: string
  project?: string
  repo?: string
  branch?: string
}

export class Gitlab {

  groupId = (g)         => `${g.name}`;
  projectId = (g, p)    => `${g.name}/${p.name}`;
  repoId = (g, p, r)    => `${g.name}/${p.name}/${r.name}`;
  objectId = (g, p, r)  => `${g.name}` + (p ? `/${p.name}` : '') + (r ? `/${r.name}` : '');

  parseProjectPath(path: string) : ProjectRef {
    let parts = path.split("/")
    let hasBranch = parts[parts.length-1].indexOf(':') != -1
    if (parts.length < 3) throw new Error(`Expected group[/subgroup]/project/repo[:branch]. Found: ${path}`)
    return {
      group: parts.slice(0, parts.length-2).join(":"),
      project: parts[parts.length-2],
      repo: parts[parts.length-1].split(":")[0],
      branch: hasBranch ? parts[parts.length-1].split(":")[1] : undefined,
    }
  }

  resolveRepository(ref: ProjectRef) : Repository {
    let found = cfg.groups?.flatMap(g => g.projects).flatMap(p => p?.repositories)
      .find(r => r?.name == ref.repo && r?.project.name == ref.project && r?.project.group.name == ref.group)
    return found as Repository
  }

  async getBranches(g, p, r) {
    let id = encodeURIComponent(this.repoId(g, p, r));
    return await load(`projects/${id}/repository/branches`);
  }

  async getGroupMembers(groupName) {
    return await load(`groups/${encodeURIComponent(groupName)}/members/all`);
  }

  async getProjectMembers(g, p, r) {
    let rid= encodeURIComponent(this.repoId(g, p, r));
    return await load(`projects/${rid}/members/all`).catch(rethrow);
  }

}

export const gitlab : Gitlab = new Gitlab();

const request = xrequest;

async function load(uri) {
  let result = [];
  let opts = req(uri, 'GET', { per_page: 100 }, {}, { resolveWithFullResponse: true });
  while (true) {
    let response = await request(opts).catch(rethrow);
    let next = response.headers["x-next-page"];
    result = result.concat(response.body);
    if (next.length === 0) break;
    opts.qs.page = next;
  }
  return result;
}

function req(uri, method = 'GET', query = {}, data = {}, options = {}) {
  return extend(
    true,
    {
      url: `${cfg.gitlab.url}/api/v4/${uri}`,
      qs: { page: 1, per_page: 100 },
      headers: { 'Private-Token': cfg.gitlab.token },
      strictSSL: false,
      method: method,
      json: true,
    },
    options,
    { qs: query },
    { formData: copyAsStrings(data) }
  );
}

function get(uri, query?) {
  return req(uri, 'GET', query);
}

function post(uri, data) {
  return req(uri, 'POST', {}, data);
}

function put(uri, data) {
  return req(uri, 'PUT', {}, data);
}

function del(uri) {
  return req(uri, 'DELETE');
}

function copyAsStrings(src, dst = {}) {
  Object.keys(src).forEach((k) => dst[k] = String(src[k]));
  return dst;
}
