import winston from 'winston';
import {is_unit_test, Main} from "./main";

export const log = winston.createLogger({
  silent: true,
});

export function configureLogging(main : Main) {
  log.silent = is_unit_test
  log.level = main.options().debug ? 'debug' : 'info'
  log.add(new winston.transports.Console({format: format(main)}))
}

function format(main: Main) {
  return winston.format.combine(
    winston.format.splat(),
    winston.format.simple(),
    winston.format.timestamp(),
    winston.format.printf(({timestamp, level, label, message}) => `${timestamp}|${lvl(level)}|${main.command.name()}| ${message}`),
  );
}

function lvl(level) {
  // dirty translation to (DBG|INF|WRN|ERR)
  return level.toLowerCase()
    .replace(/[aeou]/gi, '')
    .substring(0, 3)
    .toUpperCase()
    .replace(/RRR/gi, 'ERR');
}


// export const log = winston.createLogger({
//   silent: options().quiet,
//   level: 'info',
//   transports: [
//     options().logfile
//       ? new winston.transports.File({filename: options().logfile})
//       : new winston.transports.Console({format: format()}),
//     //cfg.elasticsearch.clientOpts.host ? new ElasticSearch(extend({ format: esformat }, cfg.elasticsearch)) : undefined,
//   ].filter(x => x),
// });






