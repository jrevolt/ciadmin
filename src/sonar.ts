import {fail, report, rethrow, xrequest} from "./utils";
import extend from "extend";
import {cfg, Group, Project, Repository} from "./cfg";
import {log} from "./log";

const request = xrequest;

export interface SonarProject {
  name: string
  repo: Repository
  branch?: string
}

interface QualityProfile {
  name: string
  language: string
  parentName: string
}


export class Sonar {

  sonarObjectId = (g:Group, p?, r?, x?) =>
    `${g.name}:${p ? p.name : ''}:${r ? r.name : ''}:${x || ''}`.replace(/:+/g, ':').replace(/:$/, '');

  projectName = (r:Repository, branch?) =>
    this.sonarObjectId(r.project.group, r.project, r, branch)

  permissionTemplateName = (r:Repository) =>
    this.sonarObjectId(r.project.group, r.project, r)

  qualityGateName = (r:Repository) =>
    this.sonarObjectId(r.project.group, r.project, r)

  qualityProfileName = (r:Repository) =>
    this.qualityProfileName0(r.project.group, r.project, r)

  qualityProfileName0 = (g:Group, p?:Project, r?:Repository) =>
    this.sonarObjectId(g, p, r)

  ownersGroupName = (r:Repository) =>
    this.ownersGroupName0(r.project.group, r.project, r)

  ownersGroupName0 = (g:Group, p?:Project, r?:Repository) =>
    this.sonarObjectId(g, p, r, 'owners')

  memberGroupName = (r:Repository) =>
    this.memberGroupName0(r.project.group, r.project, r)

  memberGroupName0 = (g:Group, p?:Project, r?:Repository) =>
    this.sonarObjectId(g, p, r, 'members')

  toSonarProject = (r:Repository, branch?) : SonarProject => Object.assign({
    name: this.projectName(r, branch),
    repo: r,
    branch: branch,
  })

  async prepareSonarProjectForCI(r:Repository, branch) {
    let ref : SonarProject = {
      name: this.projectName(r, branch),
      repo: r,
      branch: branch,
    }
    let template, gate;
    await Promise.all([
      this.makeProject(ref),
      this.makeUserGroups(r),
      this.makePermissionTemplate(this.permissionTemplateName(r)).then(x => template = x),
      this.makeQualityProfiles(r),
      this.makeQualityGate(r).then( g => gate = g),
    ])
    await Promise.all([
      this.configurePermissionTemplate(r),
      this.configureQualityProfiles(r),
    ])
    await Promise.all([
      this.applyPermissionTemplate(template.id, this.projectName(r, ref.branch)),
      this.applyQualityProfiles(r, ref),
      this.applyQualityGate(gate, ref)
    ])
  }

  async makeProject(p:SonarProject) {
    return (
      await request(get('projects/search', { projects: p.name }))
        .then(data => data.components.find(x => x.key === p.name))
        .catch(rethrow)
      ||
      await request(post(
        'projects/create',
        { name: p.name, project: p.name, visibility: "private" }))
        .then(data => data.component)
        .catch(rethrow)
    )
  }

  async makeUserGroup(name: string) {
    return (
      await request(get('user_groups/search', { q: name }))
        .then(data => data.groups.find(x => x.name === name))
        .catch(rethrow)
      ||
      await request(post('user_groups/create', { name: name }))
        .then(data => data.group)
        .catch(rethrow)
    )
  }

  async makeUserGroups(r: Repository) {
    await Promise.all([
      this.makeUserGroup(this.memberGroupName(r)),
      this.makeUserGroup(this.ownersGroupName(r)),
      this.makeUserGroup(this.ownersGroupName0(r.project.group, r.project)),
      this.makeUserGroup(this.ownersGroupName0(r.project.group)),
    ])
  }

  async makePermissionTemplate(name: string) {
    return (
      await request(get('permissions/search_templates', { q: name }))
        .then(data => data.permissionTemplates.find(x => x.name === name))
        .catch(rethrow)
      ||
      await request(post('permissions/create_template', { name: name, projectKeyPattern: `${name}(:.*)?` }))
        .then(data => data.permissionTemplate)
        .catch(rethrow)
    )
  }

  async configurePermissionTemplate(r:Repository) {
    const templateName = this.permissionTemplateName(r);
    const memberPermissions = ["user", "scan", "codeviewer"];
    const ownerPermissions = memberPermissions.concat(["admin", "issueadmin", "securityhotspotadmin"]);
    const adminPermissions = ["user", "admin"];

    await Promise.all([
      [...memberPermissions].forEachAsync(async x =>
        await this.addUserGroupToPermissionTemplate(this.memberGroupName(r), templateName, x)),
      [...ownerPermissions].forEachAsync(async x =>
        await this.addUserGroupToPermissionTemplate(this.ownersGroupName(r), templateName, x)),
      [...adminPermissions].forEachAsync(async x =>
        await this.addUserGroupToPermissionTemplate('sonar-administrators', templateName, x)),
    ]);
  }

  async addUserGroupToPermissionTemplate(groupName, templateName, permission) {
    await request(post(
      'permissions/add_group_to_template',
      { groupName: groupName, templateName: templateName, permission: permission }))
      .catch(rethrow);
  }

  async applyPermissionTemplate(templateId: string, projectKey: string) {
    await request(post(
      'permissions/apply_template',
      { projectKey: projectKey, templateId: templateId }))
      .catch(rethrow);
  }

  async makeQualityProfile(name, lang, index?) {
    return (
      index?.get(`${name}#${lang}`)
      ||
      await request(get('qualityprofiles/search', { qualityProfile: name, language: lang }))
        .then(data => data.profiles.find(x => x.name === name))
        .catch(rethrow)
      ||
      await request(post('qualityprofiles/create', { name: name, language: lang }))
        .then( async (created) => await this.getQualityProfile(name, lang))
        .catch(async (err) => {
          if (/already exists/.test(err.msg)) return await this.getQualityProfile(name, lang)
          else rethrow(err)
        })
    );
  }

  async makeQualityProfiles(r: Repository) {
    await r.options?.languages?.forEachAsync(async (lang) =>
      await Promise.all([
        this.makeQualityProfile(cfg.ci.sonar.defaultQualityProfileName, lang),
        this.makeQualityProfile(this.qualityProfileName0(r.project.group), lang),
        this.makeQualityProfile(this.qualityProfileName0(r.project.group, r.project), lang),
        this.makeQualityProfile(this.qualityProfileName0(r.project.group, r.project, r), lang),
      ]))
  }

  async createQualityProfile(name, lang, index?) {
    await request(post('qualityprofiles/create', { name: name, language: lang }))
      .catch(rethrow)
  }

  getParentName(s: string) {
    return s.replace(/(:[^:]+|^[^:]*)$/, '')
  }

  async configureQualityProfile(name, lang) {
    let profiles: string[] = []
    for (let s=name; s.length>0; s = this.getParentName(s)) profiles.push(s)
    profiles.push(cfg.ci.sonar.defaultQualityProfileName)
    let dflt = await this.getDefaultQualityProfile(lang)
    await profiles.forEachAsync(async (x) => {
      await this.makeQualityProfile(x, lang).then(async () => {
        let parent = profiles[profiles.indexOf(x) + 1] || dflt.name
        await Promise.all([
          this.updateQualityProfileParent(x, lang, parent),
          this.updateQualityProfilePermissions(x, lang),
        ])
      })
    })
  }

  async getDefaultQualityProfile(lang) : Promise<any> {
    let resp = await request(get('qualityprofiles/search', { defaults: true, language: lang }))
    return resp.profiles.first()
  }

  async getQualityProfile(name, lang) : Promise<any> {
    let resp = await request(get('qualityprofiles/search', { qualityProfile: name, language: lang }))
    if (resp.profiles.length>1) {
      log.warn(`Duplicate quality profiles found (${name}#${lang}).`)
      await resp.profiles.slice(1).forEachAsync(async (x) =>
        await this.renameQualityProfile(x.key, `${x.name}-${x.key}`))
    }
    return resp.profiles.first()
  }

  async renameQualityProfile(key, newname) {
    await request(post('qualityprofiles/rename', { key: key, name: newname }))
  }

  async configureQualityProfiles(r: Repository) {
    let name = this.qualityProfileName(r)
    await r.options?.languages?.forEachAsync(async (lang) =>
      await this.configureQualityProfile(name, lang))
  }

  async applyQualityProfile(p:SonarProject, lang:string) {
    let project = this.projectName(p.repo, p.branch)
    let profile = this.qualityProfileName(p.repo)
    await request(post(
      'qualityprofiles/add_project',
      { project: project, qualityProfile: profile, language: lang}))
      .catch(rethrow);
  }

  async applyQualityProfiles(r: Repository, ref: SonarProject) {
    r.options?.languages?.forEachAsync(async (lang) =>
      await this.applyQualityProfile(ref, lang))
  }

  qualityProfileIndexId = (name, lang)  => `${name}#${lang}`;

  async updateQualityProfileParent(name, lang, parent: string, index ?: Map<string,QualityProfile>) {
    if (index && (index.get(this.qualityProfileIndexId(name, lang))?.parentName === parent))
      return; // up to date

    await request(post(
      'qualityprofiles/change_parent',
      { qualityProfile: name, language: lang, parentQualityProfile: parent }))
      .catch(rethrow);
  }

  async updateQualityProfilePermissions(name, lang) {
    let group = `${name}:owners`;
    await this.makeUserGroup(group)
    await request(post('qualityprofiles/add_group', {
      qualityProfile: name,
      language: lang,
      group: group,
    }));
  }


  async makeQualityGate(r:Repository) {
    let name = this.qualityGateName(r);
    return (
      await request(get('qualitygates/list', { ps: 999 }))
        .then(data => data.qualitygates.find(x => x.name === name))
        .catch(rethrow)
      ||
      await request(post('qualitygates/copy', { id: 1, key: name, name: name })).catch(rethrow)
    )
  }

  async applyQualityGate(gate, project: SonarProject) {
    let projectKey = this.projectName(project.repo, project.branch)
    await request(post('qualitygates/select', { projectKey: projectKey, gateId: gate.id })).catch(report);
  }

}

export const sonar : Sonar = new Sonar();

function req(uri, method, data?, options?) {
  cfg.sonar.url || fail('cfg.sonar.url')
  cfg.sonar.token || fail('cfg.sonar.token')
  return extend(
    {
      url: `${cfg.sonar.url}/api/${uri}`,
      headers: {
        'Authorization': `Basic ${Buffer.from(`${cfg.sonar.token}:`).toString('base64')}`,
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      method: method,
      resolveWithFullResponse: false,
      json: true,
      qs: (method === 'GET') ? data : {},
      query: (method === 'GET') ? data : {},
      form: (method === 'POST') ? data : {},
    },
    options || {}
  );
}

function get(uri, data?) {
  return req(uri, 'GET', data);
}

function post(uri, data) {
  return req(uri, 'POST', data);
}

function xpost(uri, data) {
  return req(uri, 'POST', data, { resolveWithFullResponse: true });
}
