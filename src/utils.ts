import {cfg} from "./cfg";
import request from 'request-promise';
import Bottleneck from "bottleneck";
import TraceError from 'trace-error';
import extend from 'extend';
import {log} from "./log";

declare global {
  interface String {
    sonarize() : string
    truncate(max: number): string
  }
  interface Array<T> {
    forEachAsync(action:(item:T)=>void) : Promise<void>
    first() : T;
  }
  interface Map<K,V> {
    forEachAsync(action: (item: V) => void): void;
  }
}

Array.prototype.first = function () {
  return this.find(()=>true);
};

Array.prototype.forEachAsync = async function (cb) {
  await Promise.all(this.map(async (x) => await cb(x)));
};


String.prototype.truncate = function (max) {
  return this.substring(0, Math.min(this.length, max));
};

export function fail<T>(msg ?: string) : T {
  throw Error(msg)
}

export function rethrow(err) {
  if (err) throw new TraceError(err.message, err);
}

export function report(err) {
  if (err) {
    let e = new TraceError(err.message, err);
    log.debug('%s', e.stack)
    log.error('Error (reported, ignored): %s', e.message);
    reportedErrors++;
  }
}

const limiter = new Bottleneck(cfg.ciadmin.bottleneck);

async function dbgrequest(options) {
  let opts = extend(true, {}, cfg.ciadmin.request, options);
  log.debug('%s %s %s %s',
    opts.method.padEnd(4),
    opts.url,
    JSON.stringify(opts.qs || opts.query || {}),
    JSON.stringify(opts.form || opts.formData || opts.body || {}).truncate(200),
  );
  return await request(opts);
}

const enableLimiter = cfg.ciadmin.bottleneck.maxConcurrent ?? 0 > 0;

export const xrequest = enableLimiter ? limiter.wrap(dbgrequest) : dbgrequest;

declare global {
  let TraceError: TraceError;
}

export let reportedErrors = 0;

