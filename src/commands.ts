import {gitlab} from "./gitlab";
import {sonar} from "./sonar";
import {fail} from "./utils";

export interface Options {
  quiet?: boolean,
  debug?: boolean,
  logfile?: string,

}

export interface PrepareSonarOptions extends Options {
  force: boolean,
}

export async function prepareSonar(opts: PrepareSonarOptions, project: string) {
  project || fail('project')
  let ref = gitlab.parseProjectPath(project)
  let r = gitlab.resolveRepository(ref)
  r || fail(`Unsupported: ${project}`)
  await sonar.prepareSonarProjectForCI(r, ref.branch)
}

export async function anotherCommand() {

}
