import {sonar} from "./sonar";
import {cfg, Group,Project,Repository} from "./cfg";
import {report} from "./utils";
import {log} from "./log";

const repo : Repository =
  cfg.groups.flatMap(g => g.projects).flatMap(p => p?.repositories).flat()
    .find(r => r?.name == 'rtest') as Repository

const r: Repository = repo
const p: Project = r.project
const g: Group = p.group

const branch   = "master"

const sproject = sonar.toSonarProject(r, branch)

test('id: project', () => {
  expect(sonar.projectName(repo)).toBe('gtest:ptest:rtest')
  expect(sonar.projectName(repo, branch)).toBe('gtest:ptest:rtest:master')
})

test('id: permission template', () => {
  expect(sonar.permissionTemplateName(repo)).toBe('gtest:ptest:rtest')
})

test('id: owners group', () => {
  expect(sonar.ownersGroupName(r)).toBe('gtest:ptest:rtest:owners')
})

test('id: members group', () => {
  expect(sonar.memberGroupName(r)).toBe('gtest:ptest:rtest:members')
})

test('getParentName()', () => {
  expect(sonar.getParentName('g:p:r')).toBe('g:p')
  expect(sonar.getParentName('g:p')).toBe('g')
  expect(sonar.getParentName('g')).toBe('')
})

test('make project', () => {
  sonar.makeProject(sproject)
})

test('make user group', () => {
  sonar.makeUserGroup(sonar.ownersGroupName(r))
})

test('make permission template', () => {
  sonar.makePermissionTemplate(sonar.permissionTemplateName(r))
})

test('configure permission template', async () => {
  await Promise.all([
    sonar.makeUserGroup(sonar.ownersGroupName(r)),
    sonar.makeUserGroup(sonar.memberGroupName(r)),
  ])
  await sonar.configurePermissionTemplate(repo)
})

test('apply permission template', async () => {
  await sonar.makePermissionTemplate(sonar.permissionTemplateName(r)).then( async (template) =>
    await sonar.applyPermissionTemplate(template.id, sonar.projectName(r, branch)))
})

test('make quality profile', () => {
  sonar.makeQualityProfile(sonar.qualityProfileName(r), 'cs')
})

test('configure quality profile', () => {
  sonar.configureQualityProfile(sonar.qualityProfileName(r), 'cs')
})

test('apply quality profile', () => {
  sonar.makeQualityProfile(sonar.qualityProfileName(r), 'cs')
  sonar.applyQualityProfile(sproject, 'cs')
})

test('make quality gate', async () => {
  await sonar.makeQualityGate(r)
})

test('apply quality gate', async () => {
  await sonar.makeQualityGate(r).then(async (gate) =>
    await sonar.makeProject(sproject).then(async () =>
      await sonar.applyQualityGate(gate, sproject)))
})

test('duplicate quality profiles + autofix', async () => {
  await Promise.all([
    sonar.createQualityProfile("test-duplicate-profile", "cs"),
    sonar.createQualityProfile("test-duplicate-profile", "cs"),
    sonar.createQualityProfile("test-duplicate-profile", "cs"),
    sonar.createQualityProfile("test-duplicate-profile", "cs"),
    sonar.createQualityProfile("test-duplicate-profile", "cs"),
    sonar.createQualityProfile("test-duplicate-profile", "cs"),
  ]).catch(report)
  await Promise.all([
    sonar.getQualityProfile("test-duplicate-profile", "cs"),
  ])
})

