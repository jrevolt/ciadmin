import config from 'config'
import extend from 'extend'
import {RequestOptions} from "http";
import Bottleneck from "bottleneck";
import {Config} from "winston/lib/winston/config";

interface Configuration {
  gitlab: {
    url: string
    token: string
  }
  sonar: {
    url: string
    token: string
  }
  rancher: {
    url: string
    accessKey: string
    secretKey: string
  }
  nexus: {
    url: string
    username: string
    password: string
  }

  ciadmin: {
    request: RequestOptions
    bottleneck: Bottleneck.ConstructorOptions
  }

  ci: {
    sonar: {
      defaultQualityProfileName: string
    }
  }

  groups: Group[]

  resolveRepository(path: string) : Repository
}

interface Named {
  name: string
}

interface Maintained {
  maintainers?: string[]
}

interface Configured {
  settings?: any,
  options?: any
}

export interface Group extends Named, Maintained, Configured {
  parent?: Group
  projects?: Project[]
}

export interface Project extends Named, Maintained, Configured {
  group: Group
  repositories?: Repository[]
}

export interface Repository extends Named, Maintained, Configured {
  project: Project
  type?: string
}

function resolveReferences(cfg: Configuration): Configuration {
  cfg.groups?.forEach(g => g.projects?.forEach(p => {
    p.group = g
    p.repositories?.forEach(r => r.project = p)
  }))
  return cfg
}

export const cfg: Configuration = resolveReferences(extend(true, {} as Configuration, config.get('ciadmin')));
