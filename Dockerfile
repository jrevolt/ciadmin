#syntax=docker/dockerfile:1-experimental

FROM moby/buildkit:rootless as buildkit-cli-fix
USER root
RUN sed -i 's/\$@/"$@"/' /usr/bin/buildctl-daemonless.sh
USER user

FROM node:14-alpine as prepare
WORKDIR /work
COPY package*json ./
RUN npm ci -d

FROM prepare as build
COPY ./ ./
RUN npm run build

FROM node:14-alpine as runtime
WORKDIR /app
ENTRYPOINT [ "node", "main.js" ]
COPY --from=prepare /work/node_modules/ ./node_modules/
COPY --from=build /work/.build/ ./
COPY --from=build /work/config/ ./config/
ARG VERSION
RUN echo "$VERSION" > version.json
#RUN NODE_ENV=Production node main.js prepare-sonar gtest/ptest/rtest:master; exit 1
#RUN find config; exit 1

#FROM runtime
#RUN --mount=type=secret,id=ciadmin,target=/app/config/local.yaml \
#    cat config/local.yaml ;\
#    NODE_ENV=test node main.js prepare-sonar gtest/ptest/rtest:master ;\
#    echo $? ;\
#    exit 1

